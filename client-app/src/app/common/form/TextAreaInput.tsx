import React, { FC } from 'react';
import { Form, Label } from 'semantic-ui-react';

interface IProps {
  input: any;
  width: any;
  rows: number;
  placeholder: string;
  meta: any;
}

const TextAreaInput: FC<IProps> = ({
  input,
  width,
  rows,
  placeholder,
  meta: { touched, error },
}) => {
  return (
    <Form.Field error={touched && !!error} width={width}>
      <textarea rows={rows} {...input} placeholder={placeholder} />
      {touched && error && (
        <Label basic color="red">
          {error}
        </Label>
      )}
    </Form.Field>
  );
};

export default TextAreaInput;
