import React, { FC } from 'react';
import { NavLink } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { Button, Container, Menu } from 'semantic-ui-react';

const { Item } = Menu;

const NavBar: FC = () => {
  return (
    <Menu fixed="top" inverted>
      <Container>
        <Item header as={NavLink} exact to="/">
          <img
            src="/assets/logo.png"
            alt="logo"
            style={{ marginRight: '10px' }}
          />
          Reactivities
        </Item>
        <Item name="Activities" as={NavLink} to="/activities" />
        <Item>
          <Button
            as={NavLink}
            to="/createActivity"
            positive
            content="Create Activity"
          />
        </Item>
      </Container>
    </Menu>
  );
};

export default observer(NavBar);
