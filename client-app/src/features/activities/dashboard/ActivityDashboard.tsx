import React, { FC, useContext, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { Grid } from 'semantic-ui-react';
import ActivityList from './ActivityList';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import { RootStoreContext } from '../../../app/stores/rootStore';

const { Column } = Grid;

const ActivityDashboard: FC = () => {
  const rootStore = useContext(RootStoreContext);
  const { loadActivities, loadingInitial } = rootStore.activityStore;

  useEffect(() => {
    loadActivities();
  }, [loadActivities]);

  if (loadingInitial) return <LoadingComponent content={'Loading...'} />;
  return (
    <Grid>
      <Column width={10}>
        <ActivityList />
      </Column>
      <Column width={6}>
        <h2>Activity filters</h2>
      </Column>
    </Grid>
  );
};

export default observer(ActivityDashboard);
