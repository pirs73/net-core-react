# Net Core React

## Complete guide to building an app with .Net Core and React https://coursehunter.net/course/polnoe-rukovodstvo-po-sozdaniyu-prilozheniya-s-net-core-i-react

## Run server

### `dotnet run -p API/`

http://localhost:5000/api/activities

## Run client

node v12.18.4 <br />
npm v6.14.6

upgrade to node 14

### `cd client-app`

### `npm install`

### `npm run start`
